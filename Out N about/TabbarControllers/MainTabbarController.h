//
//  MainTabbarController.h
//  Out N about
//
//  Created by Ram Kumar on 11/05/16.
//  Copyright © 2016 tecHindustan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainTabbarController : UITabBarController <UITabBarControllerDelegate>

@property (strong, nonatomic) IBOutlet UIBarButtonItem *barButtonItem;


@property (nonatomic,strong) NSString *screenType;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;

@property(strong, nonatomic) UITableView *imagesTableView;


@property (retain, nonatomic) NSURLConnection *connection;

@property (retain, nonatomic) NSMutableData *receivedData;
@end
