//
//  FourthTabbarVC.h
//  Out N about
//
//  Created by Ram Kumar on 12/05/16.
//  Copyright © 2016 tecHindustan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TopEventTabbarVC : UIViewController<UITableViewDataSource,UITableViewDelegate,NSURLConnectionDataDelegate>


@property (weak, nonatomic) IBOutlet UITableView *imagesTableView;
@property (retain, nonatomic) NSURLConnection *connection;

@property (retain, nonatomic) NSMutableData *receivedData;
@end
