//
//  SingleEventVC.m
//  OutNAbout
//
//  Created by Ram Kumar on 22/06/16.
//  Copyright © 2016 tecHindustan. All rights reserved.
//

#import "SingleEventVC.h"
#import "BIZPopupViewController.h"
#import "PopUpVC.h"
#import "SDWebImageManager.h"


@interface SingleEventVC ()<MKMapViewDelegate,CLLocationManagerDelegate>
@property (nonatomic, strong) CLLocation* currentLocation;
@property (weak, nonatomic) IBOutlet UIView *bottomView;


#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE_6 ( fabs ((double)[[UIScreen mainScreen] bounds].size.height - (double) 667) < DBL_EPSILON)
#define API_URL @"http://agiledevelopers.in/outnabout/webservices/users/"

@end

@implementation SingleEventVC

{
    CLLocationManager *locationManager;
    CLGeocoder *geocoder;
    CLPlacemark *placemark;
    MKPointAnnotation *annotation;
    BOOL checkButtonSelected;
     NSString *access_token;
    int favButtonTapType;
    NSString *savedEventId;
   // CLLocation *CurrentLocation;
}

@synthesize imageView,scrollView,mapView,singleTitle,singleImageString,singleCategoryString,startDateOrTimeString,endDateOrTimeString,venueString,costString,descriptionString,viewsString,favoriteString,eventIdString,commentsString,favsString,userNameString,userCommentString;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    favButtonTapType=0;
    
    _favButton.imageEdgeInsets = UIEdgeInsetsMake(5, 5, 5, 5);
    _favButton.contentMode = UIViewContentModeScaleToFill;

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    access_token = [defaults objectForKey:@"access_token"];
    
    
    UIBarButtonItem *_barButtonItem = [[UIBarButtonItem alloc] initWithTitle:[NSString stringWithFormat:@"<  %@",singleTitle]
                                                                       style:UIBarButtonItemStylePlain target:self action:@selector(handleBack:)];
    _barButtonItem.tintColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItem = _barButtonItem;
    
    self.navigationItem.hidesBackButton = YES;
    
    [self setImageOnScreenWithAnimationAndShade];
    
    
    
    _singleTitleLabel.text = singleTitle;
    _singleTitleLabel.textColor = [UIColor whiteColor];
    _singleTitleLabel.font = [UIFont fontWithName:@"Avenir-Medium" size:22.0f];
    
    _singleCatField.text = [NSString stringWithFormat:@"%@",singleCategoryString];
    _singleCatField.borderStyle = UITextBorderStyleLine;
    _singleCatField.font= [UIFont systemFontOfSize:12.0f];
    _singleCatField.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    _singleCatField.backgroundColor = [UIColor clearColor];
    [_singleCatField setAllowsEditingTextAttributes:NO];
    _singleCatField.textColor =[UIColor whiteColor];
    _singleCatField.layer.borderColor = [UIColor whiteColor].CGColor;
    _singleCatField.layer.borderWidth = 1.0f;
    
    _startDateOrTime.text = startDateOrTimeString;
    _startDateOrTime.textColor = [UIColor blackColor];
    _startDateOrTime.font = [UIFont systemFontOfSize:13.0f];
        
    _endDateOrTime.text = endDateOrTimeString;
    _endDateOrTime.textColor = [UIColor blackColor];
    _endDateOrTime.font = [UIFont systemFontOfSize:13.0f];
        
    _venueLabel.text = venueString;
    _venueLabel.textColor = [UIColor whiteColor];
    _venueLabel.font = [UIFont systemFontOfSize:14.0f];
    
    _costLabel.text = [NSString stringWithFormat:@"Cost: %@",costString];
    _costLabel.textColor = [UIColor blackColor];
    _costLabel.font = [UIFont systemFontOfSize:13.0f];
 
    
    NSScanner *myScanner;
    NSString *text = nil;
    myScanner = [NSScanner scannerWithString:descriptionString];
    
    while ([myScanner isAtEnd] == NO) {
        
        [myScanner scanUpToString:@"<" intoString:NULL] ;
        
        [myScanner scanUpToString:@">" intoString:&text] ;
        
        descriptionString = [descriptionString stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@>", text] withString:@""];
    }
    
    descriptionString = [descriptionString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    _descriptionView.text = descriptionString;
    _descriptionView.textColor = [UIColor blackColor];
    _descriptionView.editable=NO;
    _descriptionView.font = [UIFont systemFontOfSize:13.0f];

    
    

    //CGSize scrollableSize = CGSizeMake(scrollView.frame.size.width, 680);
  //  [scrollView setContentSize:scrollableSize];
  //  [scrollView setAlwaysBounceHorizontal:NO];
    
    if (IS_IPHONE_5) {
        scrollView.contentSize =CGSizeMake(320, 780);

    }
    else{
        scrollView.contentSize =CGSizeMake(320, 900);

    }
    scrollView.bounces = NO;
    //CGPoint bottomOffset = CGPointMake(0, scrollView.contentSize.height - scrollView.bounds.size.height);
    //[scrollView setContentOffset:bottomOffset animated:YES];

    
    [self createMapOnScreen];
    
}


- (void) handleBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}








-(void)setImageOnScreenWithAnimationAndShade
{
    //  UIImage * img = [UIImage imageWithData:[NSData dataWithContentsOfURL:     [NSURL URLWithString:singleImageString]]];
    // [imageView setImage:img];
    
    NSURL *url =[NSURL URLWithString:singleImageString];
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:url
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger  expectedSize) {
                             // progression tracking code
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            if (image) {
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    
                                    
                                    imageView.alpha = 0.0;
                                    [UIView transitionWithView:imageView
                                                      duration:0.4
                                                       options:UIViewAnimationOptionTransitionCrossDissolve
                                                    animations:^{
                                                        [imageView setImage:image];
                                                        imageView.alpha = 1.0;
                                                    } completion:NULL];
                                    
                                    
                                    CAGradientLayer *grad = [CAGradientLayer layer];
                                if (IS_IPHONE_5) {
                                grad.frame = CGRectMake(0, imageView.frame.size.height/2 , imageView.frame.size.width, 92);

                                    }
                                else
                                {
                                     grad.frame = CGRectMake(0, imageView.frame.size.height/2 , imageView.frame.size.width, 110);
                                    }
                                    grad.colors = [NSArray arrayWithObjects:(id)[[UIColor clearColor] CGColor], (id)[[UIColor blackColor] CGColor],nil];
                                    
                                    [imageView.layer insertSublayer:grad atIndex:0];
                                    
                                    
                                });
                            }
                            
                            
                            else{
                                
                                CAGradientLayer *grad = [CAGradientLayer layer];
                                grad.frame = CGRectMake(0, imageView.frame.size.height/2 , imageView.frame.size.width, 110);
                                grad.colors = [NSArray arrayWithObjects:(id)[[UIColor clearColor] CGColor], (id)[[UIColor blackColor] CGColor],nil];
                                
                                [imageView.layer insertSublayer:grad atIndex:0];
                                
                            }
                        }
     ];

}







-(void)createMapOnScreen
{
    mapView.showsUserLocation = YES;
    [mapView setZoomEnabled :YES];
    [mapView setScrollEnabled:YES];
    
    mapView.mapType = MKMapTypeStandard;
    mapView.delegate = self;
    [scrollView addSubview:mapView];
    
    
    locationManager = [CLLocationManager new];
    locationManager.delegate = self;
    
    if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
    {
        [locationManager requestWhenInUseAuthorization];
        mapView.showsUserLocation = YES;
    }
    locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
    locationManager.activityType = CLActivityTypeFitness;
    locationManager.distanceFilter = 500;
    [locationManager startUpdatingLocation];
}




- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
        _currentLocation = [locations objectAtIndex:0];
        [locationManager stopUpdatingLocation];
        geocoder = [[CLGeocoder alloc] init];
    
    
        CLLocationCoordinate2D coordinate = [_currentLocation coordinate];
    
    
    
        NSString *latitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
        NSString *longitude = [NSString stringWithFormat:@"%f", coordinate.longitude];
    
        NSString *display_coordinates=[NSString stringWithFormat:@"Latitude %f and Longitude %f",coordinate.longitude,coordinate.latitude];
    
        annotation = [[MKPointAnnotation alloc] init];
        [annotation setCoordinate:coordinate];
    
        NSLog(@"latitude :%@",latitude);
        NSLog(@"dLongitude : %@",longitude);
    
        [geocoder reverseGeocodeLocation:_currentLocation completionHandler:^(NSArray *placemarks, NSError *error)
         {
             if (!(error))
             {
                 placemark = [placemarks objectAtIndex:0];
                 NSLog(@"\nCurrent Location Detected\n");
                 NSLog(@"placemark %@",placemark);
                 NSString *locatedAt = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
                 NSString *Address = [[NSString alloc]initWithString:locatedAt];
                 NSString *Area = [[NSString alloc]initWithString:placemark.locality];
                 NSString *Country = [[NSString alloc]initWithString:placemark.country];
                 NSString *CountryArea = [NSString stringWithFormat:@"%@, %@", Area,Country];
                 NSLog(@"%@",CountryArea);
    
                 [annotation setTitle:Area];
                 [annotation setSubtitle:display_coordinates];
                 [mapView addAnnotation:annotation];
    
             }
             else
             {
                 NSLog(@"Geocode failed with error %@", error);
                 NSLog(@"\nCurrent Location Not Detected\n");

             }

         }];
 
}




- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id < MKOverlay >)overlay
{
    if ([overlay isKindOfClass:[MKPolyline class]]) {
        MKPolyline *polyLine = (MKPolyline *)overlay;
        MKPolylineRenderer *aRenderer = [[MKPolylineRenderer alloc] initWithPolyline:polyLine];
        aRenderer.strokeColor = [UIColor blueColor];
        aRenderer.lineWidth = 3;
        return aRenderer;
    }
    
    return nil;
}








- (void)mapView:(MKMapView *)aMapView didUpdateUserLocation:(MKUserLocation *)aUserLocation
{
    MKCoordinateRegion region;
    MKCoordinateSpan span;
    span.latitudeDelta = 0.005;
    span.longitudeDelta = 0.005;
    CLLocationCoordinate2D location;
    location.latitude = aUserLocation.coordinate.latitude;
    location.longitude = aUserLocation.coordinate.longitude;
    region.span = span;
    region.center = location;
    [aMapView setRegion:region animated:YES];
}



- (IBAction)addEventToCalendarAction:(id)sender
{
    
    EKEventStore *store = [EKEventStore new];
    [store requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error)
     {
        if (!granted) {
            return;
        }
        EKEvent *event = [EKEvent eventWithEventStore:store];
        event.title = @"Event Title";
        event.startDate = [NSDate date]; //today
        event.endDate = [event.startDate dateByAddingTimeInterval:60*60];  //set 1 hour meeting
        event.calendar = [store defaultCalendarForNewEvents];
        NSError *err = nil;
        [store saveEvent:event span:EKSpanThisEvent commit:YES error:&err];
        savedEventId = event.eventIdentifier;  //save the event id if you want to access this later
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"calshow://"]];
        NSLog(@"calendar string** :%@",savedEventId);
    }];
    
    
    
    
    
    
//    EKEventStore *eventStore = [[EKEventStore alloc] init];
//    if ([eventStore respondsToSelector:@selector(requestAccessToEntityType:completion:)])
//    {
//        // the selector is available, so we must be on iOS 6 or newer
//        [eventStore requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
//            dispatch_async(dispatch_get_main_queue(), ^{
//                if (error)
//                {
//                    // display error message here
//                }
//                else if (!granted)
//                {
//                    // display access denied error message here
//                }
//                else
//                {
//                    // access granted
//                    // ***** do the important stuff here *****
//                }
//            });
//        }];
//    }
//    else
//    {
//        // this code runs in iOS 4 or iOS 5
//        // ***** do the important stuff here *****
//    }

    
    
    
    
    
//    EKEventStore *eventStore=[[EKEventStore alloc] init];
//    
//    [eventStore requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error)
//     {
//         if (granted)
//         {
//             EKEvent *event  = [EKEvent eventWithEventStore:eventStore];
//             NSString * NoteDetails =@"Event detail";
//             
//             NSDate *startDate = [NSDate date];
//             
//             //Create the end date components
//             NSDateComponents *tomorrowDateComponents = [[NSDateComponents alloc] init];
//             tomorrowDateComponents.day = 1;
//             
//             NSDate *endDate = [[NSCalendar currentCalendar] dateByAddingComponents:tomorrowDateComponents
//                                                                             toDate:startDate
//                                                                            options:0];
//             
//             event.title =@"Your Event TITLE";
//             event.startDate=startDate;
//             event.endDate=endDate;
//             event.notes = @"hitesh";
//             event.allDay=YES;
//             
//             [event setCalendar:[eventStore defaultCalendarForNewEvents]];
//             
//             NSError *err;
//             [eventStore saveEvent:event span:EKSpanThisEvent error:&err];
//         }
//         else
//         {
//             NSLog(@"NoPermission to access the calendar");
//         }
//         
//     }];
   ///
    //[self setiCalEvent];
    
}




//-(void) setiCalEvent {
//    
//    // Set the Date and Time for the Event
//    NSDateComponents *comps = [[NSDateComponents alloc] init];
//    [comps setYear:2013];
//    [comps setMonth:3];
//    [comps setDay:5];
//    [comps setHour:9];
//    [comps setMinute:0];
//    [comps setTimeZone:[NSTimeZone systemTimeZone]];
//    NSCalendar *cal = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
//    NSDate *eventDateAndTime = [cal dateFromComponents:comps];
//    
//    // Set iCal Event
//    EKEventStore *eventStore = [[EKEventStore alloc] init];
//    
//    EKEvent *event = [EKEvent eventWithEventStore:eventStore];
//    event.title = @"EVENT TITLE";
//    
//    event.startDate = eventDateAndTime;
//    event.endDate = [[NSDate alloc] initWithTimeInterval:600 sinceDate:event.startDate];
//    NSLog(@"########");
//    // Check if App has Permission to Post to the Calendar
//    [eventStore requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
//        if (granted){
//            //---- code here when user allows your app to access their calendar.
//            [event setCalendar:[eventStore defaultCalendarForNewEvents]];
//            NSError *err;
//            [eventStore saveEvent:event span:EKSpanThisEvent error:&err];
//            
//            
//            NSLog(@"@@@@@@@@@@");
//        }else
//        {
//            NSLog(@"*******");
//            //----- code here when user does NOT allow your app to access their calendar.
//            [event setCalendar:[eventStore defaultCalendarForNewEvents]];
//            NSError *err;
//            [eventStore saveEvent:event span:EKSpanThisEvent error:&err];
//        }
//    }];
//}





- (IBAction)actionTypeRSVP:(id)sender {
}





- (IBAction)addCommentAction:(id)sender {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    PopUpVC *smallViewController = [storyboard instantiateViewControllerWithIdentifier:@"bigViewController"];
    smallViewController.eventIdString = eventIdString;
    
    BIZPopupViewController *popupViewController = [[BIZPopupViewController alloc] initWithContentViewController:smallViewController contentSize:CGSizeMake(300, 400)];
    [self presentViewController:popupViewController animated:NO completion:nil];
}



- (IBAction)favoriteButtonAction:(id)sender
{
    favButtonTapType=1;
    
    [self postMethodForFavorite];
}



-(void)postMethodForFavorite
{
    
    [self.connection cancel];
    
    //initialize new mutable data
    NSMutableData *data = [[NSMutableData alloc] init];
    self.receivedData = data;
    
    NSURL *url = [NSURL URLWithString:(API_URL @"updatefavourite")];
    
    //initialize a request from url
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL]];
    
    //set http method
    [request setHTTPMethod:@"POST"];
    //initialize a post data
    
    NSString *postData = [NSString stringWithFormat:@"event_id=%@ & app_token=%@ ", eventIdString,access_token];
    
    
    [request setValue:@"application/x-www-form-urlencoded; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    
    [request setHTTPBody:[postData dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    self.connection = connection;
    
    //start the connection
    [connection start];
    
    
    
}






-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    
    [self postMethodForSingleEventDetail];
}


-(void)postMethodForSingleEventDetail
{
    
    [self.connection cancel];
    
    //initialize new mutable data
    NSMutableData *data = [[NSMutableData alloc] init];
    self.receivedData = data;
    
    NSURL *url = [NSURL URLWithString:(API_URL @"singleevent")];
    
    //initialize a request from url
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL]];
    
    //set http method
    [request setHTTPMethod:@"POST"];
    //initialize a post data
    
    NSString *postData = [NSString stringWithFormat:@"event_id=%@ & app_token=%@ ", eventIdString,access_token];
    
    
    [request setValue:@"application/x-www-form-urlencoded; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    
    [request setHTTPBody:[postData dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    self.connection = connection;
    
    //start the connection
    [connection start];

    
    
}


#pragma mark - NSURL Post methods 


-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    [self.receivedData appendData:data];
}


-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"eeeeeeee  %@" , error);
}


-(void)connectionDidFinishLoading:(NSURLConnection *)connection{
    
    //initialize convert the received data to string with UTF8 encoding
    NSString *htmlSTR = [[NSString alloc] initWithData:self.receivedData
                                              encoding:NSUTF8StringEncoding];
    // NSLog(@" ffff     %@" , htmlSTR);
    
    NSError *error = nil;
    NSDictionary *jsonArray = [NSJSONSerialization JSONObjectWithData: self.receivedData options: NSJSONReadingMutableContainers error: &error];  //I am using sbjson to parse
    
    NSDictionary *data = jsonArray[@"data"];
    viewsString = data[@"views"];
    commentsString = data[@"commentcount"];
    favsString = data[@"favcount"];
    
    NSMutableArray *commenting =[data valueForKey:@"allcommrating"];
    if (commenting.count)
    {
        NSDictionary *d= [commenting objectAtIndex:0];
        userNameString = d[@"user_name"];
        userCommentString = d[@"comment"];
       // NSLog(@"response name *** :%@",userNameString);
        _userNameLabel.hidden = NO;
        _noReviewLabel.hidden=YES;
        _userCommentSection.hidden=NO;

    }
    
    else{
        _noReviewLabel.hidden=NO;
        _userNameLabel.hidden = YES;
        _userCommentSection.hidden=YES;
    }
    
    NSLog(@"response :%@",commenting);

//    for (NSDictionary *c in commenting) {
//        NSString *userName = c[@"user_name"];
//        userNameString =userName;
//       // userNameString =[commenting valueForKey:@"user_name"];
//    }
 
//    NSLog(@"response *** :%@",userNameString);
    
    if (favButtonTapType==0) {
    NSString *favString = data[@"favorite"];
    NSNumber  *favNo = [NSNumber numberWithInteger: [favString integerValue]];
    [self singleEventDetailLabel];

    
    if ([favNo isEqualToNumber:[NSNumber numberWithInteger:0]]) {
        [_favButton setImage:[UIImage imageNamed:@"ic_star_disable@3x.png"] forState:UIControlStateNormal];
    }
    else{
        [_favButton setImage:[UIImage imageNamed:@"ic_star_enable@3x.png"] forState:UIControlStateNormal];
    }
    }
    
    
    
    else if (favButtonTapType==1) {
      
    
    NSString *favValue = jsonArray[@"favorite"];
    NSLog(@"fav value******* :%@",favValue);
    NSNumber  *favNumber = [NSNumber numberWithInteger: [favValue integerValue]];

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:favNumber forKey:@"fav"];
    
    if ([favNumber isEqualToNumber:[NSNumber numberWithInteger:1]]) {
        [_favButton setImage:[UIImage imageNamed:@"ic_star_enable@3x.png"] forState:UIControlStateNormal];
    }
    else{
        [_favButton setImage:[UIImage imageNamed:@"ic_star_disable@3x.png"] forState:UIControlStateNormal];

    }
        
    }
    
    else
    {
        
    }
    
}




-(void)singleEventDetailLabel
{
    _viewsLabel.text = [NSString stringWithFormat:@"%@ Views",viewsString];
    _viewsLabel.textColor = [UIColor whiteColor];
    _viewsLabel.font = [UIFont systemFontOfSize:12.0f];
    
    _totalCommentsLabel.text = [NSString stringWithFormat:@"%@ ",commentsString];
    _totalCommentsLabel.textColor = [UIColor whiteColor];
    _totalCommentsLabel.font = [UIFont systemFontOfSize:12.0f];
    
    _favCountLabel.text = [NSString stringWithFormat:@"%@ People",favsString];
    _favCountLabel.textColor = [UIColor whiteColor];
    _favCountLabel.font = [UIFont systemFontOfSize:12.0f];
    
    
    _userNameLabel.text = [NSString stringWithFormat:@"%@",userNameString];
    _userNameLabel.textColor = [UIColor blackColor];
    _userNameLabel.font = [UIFont systemFontOfSize:12.0f];

    _userCommentSection.text = [NSString stringWithFormat:@"%@",userCommentString];
    _userCommentSection.textColor = [UIColor blackColor];
    _userCommentSection.editable=NO;
    _userCommentSection.font = [UIFont systemFontOfSize:12.0f];

}





//- (void)textViewDidChange:(UITextView *)textView
//{
//    CGFloat fixedWidth = textView.frame.size.width;
//    CGSize newSize = [textView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
//    CGRect newFrame = textView.frame;
//    newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
//    textView.frame = newFrame;
//}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}



@end
