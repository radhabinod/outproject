//
//  SearchEventsVC.m
//  OutNAbout
//
//  Created by Ram Kumar on 21/07/16.
//  Copyright © 2016 tecHindustan. All rights reserved.
//

#import "SearchEventsVC.h"
#import "SWRevealViewController.h"

@interface SearchEventsVC ()

@end

@implementation SearchEventsVC

- (void)viewDidLoad {
    [super viewDidLoad];
  
    UIBarButtonItem *_barButtonItem = [[UIBarButtonItem alloc] initWithTitle:[NSString stringWithFormat:@"<  Settings"]
                                                                       style:UIBarButtonItemStylePlain target:self action:@selector(handleBack:)];
    _barButtonItem.tintColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItem = _barButtonItem;
    
    self.navigationItem.hidesBackButton = YES;
}


- (void) handleBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}



@end
