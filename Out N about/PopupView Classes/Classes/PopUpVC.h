//
//  PopUpVC.h
//  OutNAbout
//
//  Created by Ram Kumar on 15/07/16.
//  Copyright © 2016 tecHindustan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RateView.h"


@interface PopUpVC : UIViewController<RateViewDelegate>
@property (weak, nonatomic) IBOutlet UITextView *commentTV;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;


@property (retain, nonatomic) NSURLConnection *connection;

@property (retain, nonatomic) NSMutableData *receivedData;


@property(nonatomic,strong) NSString *eventIdString;

@property (weak, nonatomic) IBOutlet RateView *rateView;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UILabel *enterCommentLabel;
@end
