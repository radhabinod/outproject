//
//  NavigationViewController.m
//  SlideOutMenu
//
//  Created by Jared Davidson on 7/14/14.
//  Copyright (c) 2014 Archetapp. All rights reserved.
//

#import "NavigationViewController.h"
#import "SWRevealViewController.h"
#import "ViewController.h"

#import "MainTabbarController.h"

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface NavigationViewController ()




@end

@implementation NavigationViewController
{
    NSArray *menu;
    NSArray *menuIcons;
    
    NSString *firstName;
    NSString *lastName;
    UIImageView *proflePic;
}


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
   //  self.navigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    
    menu = @[@"", @"All Events", @"My Events", @"My Favorite Events",@"Events History",@"Create Event", @"Settings", @"Logout"];
    
    menuIcons = @[@"ic_all_event.png",@"ic_my_events.png",@"ic_history.png",@"ic_setting.png",@"ic_setting.png",@"ic_history.png",@"ic_logout.png",@"ic_history.png"];
    
 }


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    // Return the number of rows in the section.
    return [menuIcons count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
   // NSString *cellIdentifier = [menu objectAtIndex:indexPath.row];
    NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell==nil)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(50.0f, 10.0f, 150.0f, 20.0f)];
        lbl.tag = 101; // define this as a constant
        [cell.contentView addSubview:lbl];
    }
    
    UILabel *lbl = (UILabel *)[cell.contentView viewWithTag:101];
    lbl.textColor =[UIColor whiteColor];
    [lbl setText:[menu objectAtIndex:indexPath.row]];
    
    //cell.textLabel.text = [menu objectAtIndex:indexPath.row];
    
    cell.textLabel.textColor = [UIColor whiteColor];
    
    
    
    if (indexPath.row==0) {
     cell.backgroundColor = [UIColor colorWithRed:0.00 green:0.20 blue:0.40 alpha:1.0];
        cell.backgroundView= [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"xxxhdpi_pbg.png"]];
        
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *loginType = [defaults objectForKey:@"loginType"];
        
        proflePic = [[UIImageView alloc]initWithFrame:CGRectMake(90, 30, 70, 70)];
       // proflePic.contentMode = UIViewContentModeScaleAspectFit;
        proflePic.layer.cornerRadius =proflePic.frame.size.width / 2;
        proflePic.clipsToBounds = YES;
        
        if ([loginType isEqualToString:@"fb"]) {

            
        FBSDKProfilePictureView *profilePictureview = [[FBSDKProfilePictureView alloc]initWithFrame:proflePic.frame];
        profilePictureview.layer.cornerRadius = 40.0f;
        profilePictureview.layer.masksToBounds = YES;
         //[proflePic setImage:[UIImage imageNamed:@"event_default.jpg"]];
        [cell.contentView addSubview:proflePic];
        [cell.contentView addSubview:profilePictureview];
            
            firstName = [defaults objectForKey:@"fbFirstName"];
            lastName = [defaults objectForKey:@"fbLastName"];
        }
        
        else{
        firstName = [defaults objectForKey:@"first_name"];
        lastName = [defaults objectForKey:@"last_name"];
        [proflePic setImage:[UIImage imageNamed:@"event_default.jpg"]];
        [cell.contentView addSubview:proflePic];

        }
        UILabel *nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(12, 100, 230, 30)];
        nameLabel.text = [NSString stringWithFormat:@"%@ %@",firstName,lastName];
        nameLabel.textColor = [UIColor whiteColor];
        nameLabel.font = [UIFont systemFontOfSize:15.0f];
        nameLabel.backgroundColor = [UIColor clearColor];
        nameLabel.textAlignment = NSTextAlignmentCenter;
        [cell.contentView addSubview:nameLabel];


    } else
    {
        UIImageView *listIcons = [[UIImageView alloc]initWithFrame:CGRectMake(10.0f, 10.0f, 20.0f, 20.0f)];
        [listIcons setImage:[UIImage imageNamed:[menuIcons objectAtIndex:indexPath.row]]];
        [cell.contentView addSubview:listIcons ];

    cell.backgroundColor  = [UIColor colorWithRed:0.12 green:0.11 blue:0.20 alpha:1.0];
        
            }
    return cell;
    
    
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==0)
    {
        NSLog(@"0");
    }
    else if (indexPath.row==1)
    {
        NSLog(@"1");
        
        [self performSegueWithIdentifier:@"toMainEventsView" sender:nil];
    }
    else if (indexPath.row==2)
    {
        [self performSegueWithIdentifier:@"toMyEventsView" sender:nil];

    }else if (indexPath.row==3)
    {
        [self performSegueWithIdentifier:@"toFavoriteView" sender:nil];
    }
    else if (indexPath.row==5)
    {
        [self performSegueWithIdentifier:@"toCreateEventView" sender:nil];
    }
    else if (indexPath.row==6)
    {
        [self performSegueWithIdentifier:@"toSettingView" sender:nil];
    }
    else if (indexPath.row==7)
    {
          [self performSegueWithIdentifier:@"toViewFromLogout" sender:nil];
    }
}



// In a storyboard-based application, you will often want to do a little preparation before navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
        if ([segue.identifier isEqualToString:@"toViewFromLogout"]) {
            //    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
            MainTabbarController *destViewController = segue.destinationViewController;
            destViewController.screenType =@"LogoutNav";
            // destViewController.recipeName = [recipes objectAtIndex:indexPath.row];
        }

    
    if ( [segue isKindOfClass: [SWRevealViewControllerSegue class]] ) {
        SWRevealViewControllerSegue *swSegue = (SWRevealViewControllerSegue*) segue;
        
        swSegue.performBlock = ^(SWRevealViewControllerSegue* rvc_segue, UIViewController* svc, UIViewController* dvc) {
            
            UINavigationController* navController = (UINavigationController*)self.revealViewController.frontViewController;
            [navController setViewControllers: @[dvc] animated: NO ];
            [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
        };
        
    }



}


- (IBAction)dd:(id)sender {
}
- (IBAction)namee:(id)sender {
}
@end
