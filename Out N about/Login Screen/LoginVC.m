//
//  LoginVC.m
//  Out N about
//
//  Created by Ram Kumar on 03/05/16.
//  Copyright © 2016 tecHindustan. All rights reserved.
//

#import "LoginVC.h"
#import "ViewController.h"
#import "MainTabbarController.h"
#import "AllEventsTabBarVC.h"


#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>



@interface LoginVC ()<NSURLConnectionDataDelegate,FBSDKLoginButtonDelegate,UITextFieldDelegate,FBSDKGraphRequestConnectionDelegate>

#define API_URL @"http://agiledevelopers.in/outnabout/webservices/users/"

@end

@implementation LoginVC
{
    int loginInteger;
    UIAlertController * alertForFields;
    
    NSMutableArray *infoArray;
    NSArray *data;
    NSString *accesstoken;
    
    NSString *firstName;
    NSString *lastName;
    NSString *email;
    
    NSString *postData;
}
@synthesize usernameTF,passwordTF,loginButtonObject;

- (void)viewDidLoad
{
    [super viewDidLoad];
    loginInteger = 0;
    
    infoArray = [[NSMutableArray alloc]init];
    
    usernameTF.delegate = self;
    passwordTF.delegate = self;
    
    [usernameTF.layer setMasksToBounds:YES];
    [usernameTF.layer setCornerRadius:5.0f];
    
    [passwordTF.layer setMasksToBounds:YES];
    [passwordTF.layer setCornerRadius:5.0f];
    
    
    [GIDSignIn sharedInstance].uiDelegate = self;
    

    
    UIImageView *backgroundImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [backgroundImage setImage:[UIImage imageNamed:@"login_bg.jpg"]];
    [self.view insertSubview:backgroundImage atIndex:0];

    
    //self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"login_bg.jpg"]];
    
    
//    FBSDKLoginButton *loginButton = [[FBSDKLoginButton alloc] init];
//   // loginButton.center = self.view.center;
//    loginButton.frame = CGRectMake(100, 460, 180, 40);
//   // [loginButton setImage:[UIImage imageNamed:@"login_fb.png"] forState:UIControlStateNormal];
//    loginButton.readPermissions =
//    @[@"public_profile", @"email", @"user_friends"];
//    [loginButton setTranslatesAutoresizingMaskIntoConstraints:NO];
//    [loginButton addTarget:self action:@selector(loginButtonClicked) forControlEvents:UIControlEventTouchUpInside];
//    [self.view addSubview:loginButton];
    

}




- (IBAction)actionToLoginFacebook:(id)sender {
    
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login
     logInWithReadPermissions: @[@"public_profile"]
     fromViewController:self
     handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
         if (error) {
             NSLog(@"Process error");
             
             [[[UIAlertView alloc]initWithTitle:@"Message" message:@"Failed login with facebook!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
         } else if (result.isCancelled) {
             NSLog(@"Cancelled");
             
             [[[UIAlertView alloc]initWithTitle:@"Message" message:@"Cancel login with facebook!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
         } else {
             NSLog(@"Logged in");
             
             
             
             [self getFacebookUserdetails];
             
             
             loginInteger = 1;
             
//             UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//             AllEventsTabBarVC *profileVCObject = [story instantiateViewControllerWithIdentifier:@"allEventsTab"];
             NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
             [defaults setInteger:loginInteger forKey:@"isloggedIn"];
             
             [defaults synchronize];
        //     [self.navigationController pushViewController:profileVCObject animated:YES];

             [self dismissViewControllerAnimated:YES completion:nil];
             
            }
         
     }];

}






-(void)getFacebookUserdetails{

#pragma mark- To Get User Info & Profile Picture==============================================================================
if ([FBSDKAccessToken currentAccessToken])
{
    FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc]
                                  initWithGraphPath:@"me" parameters:@{@"fields": @"id, name, link, first_name, last_name, picture.type(large), email,age_range, gender,birthday,friends"} HTTPMethod:@"GET"];
    [request   startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
        
//        UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(110, 50, 100, 100)];
//        [self.view addSubview:imageView];
//        
//        FBSDKProfilePictureView *profilePictureview = [[FBSDKProfilePictureView alloc]initWithFrame:imageView.frame];
//        [profilePictureview setProfileID:result[@"id"]];
//        [self.view addSubview:profilePictureview];
        
//        emailLabel = [[UILabel alloc]initWithFrame:CGRectMake(100, 150, 200, 60)];
//        emailLabel.font=[UIFont fontWithName:@"Baskerville-SemiBold" size:20.0f];
//        emailLabel.text = [result  objectForKey:@"name"];
//        emailLabel.userInteractionEnabled=YES;
//        [emailLabel setTextColor:[UIColor blackColor]];
//        [self.view addSubview:emailLabel];
        
#pragma mark-There are all labels in which we will store the user data==================================================
        
//        birthdayLabel = [[UILabel alloc]initWithFrame:CGRectMake(70, 200, 200, 60)];
//        birthdayLabel.userInteractionEnabled=YES;
//        birthdayLabel.text = [result objectForKey:@"email"];
//        [self.view addSubview:birthdayLabel];
        
        if (!error)
        {
            NSString *accessToken =[FBSDKAccessToken currentAccessToken].tokenString;
            NSString *surl = @"https://graph.facebook.com/v2.5/131180217248846/friends?access_token=%@&fields=picture,name";
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:surl,accessToken]];
            NSURLRequest *request = [NSURLRequest requestWithURL:url];
            [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc]init] completionHandler:^(NSURLResponse *r , NSData *d, NSError *e){
                if (e==nil)
                {
                    NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:d options:NSJSONReadingMutableContainers error:nil];
                    infoArray=[NSMutableArray arrayWithArray:jsonData[@"data"]];
                    
                    dispatch_async(dispatch_get_main_queue(),^{
                   //     [self.tableView reloadData];
                    });
                }
            }];
            
            //self.tableView.delegate = self;
           // self.tableView.dataSource = self;
            NSLog(@"fetched user and Name : %@ Email:%@ Gender:%@ Age:%@ Birday:%@ Friends:%@ ",[result objectForKey:@"name"],[result objectForKey:@"email"],[result objectForKey:@"gender"],[result objectForKey:@"age_range"],[result objectForKey:@"birthday"],[result objectForKey:@"friends"]);
            
            NSString *fbFirst_Name = [result objectForKey:@"first_name"];
            NSString *fbLast_Name  = [result objectForKey:@"last_name"];
            NSString *fbEmail = [result objectForKey:@"email"];
            NSString *fbId = [result objectForKey:@"id"];
            NSString *fbProfilePic = [result objectForKey:@"picture.type(large)"];
            
            NSString *loginType = @"fb";

            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject:fbFirst_Name forKey:@"fbFirstName"];
            [defaults setObject:fbLast_Name forKey:@"fbLastName"];
            [defaults setObject:fbId forKey:@"fbID"];
            [defaults setObject:fbEmail forKey:@"fbemail"];
            [defaults setObject:loginType forKey:@"loginType"];
            [defaults setObject:fbProfilePic forKey:@"fbProfilepic"];
            [defaults synchronize];
            
            [self socialFacebookOrGooglePostMethod:fbFirst_Name last:fbLast_Name emails:fbEmail id:fbId logintype:loginType];
            //[self socialFacebookOrGooglePostMethod];
            
        }
        
        

    }];
    
    
}




}


- (IBAction)actionToLoginGoogle:(id)sender {
    
    [[GIDSignIn sharedInstance] signIn];
    
    
    
  //  NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
   
   // [self dismissViewControllerAnimated:YES completion:nil];
    
    
    
    
    
    //
//
}


#pragma mark - Google Sign in delegates methods 

//- (void)signIn:(GIDSignIn *)signIn
//didSignInForUser:(GIDGoogleUser *)user
//     withError:(NSError *)error {
//    
//    // Perform any operations on signed in user here.
//    NSString *userId = user.userID;                  // For client-side use only!
//    NSString *idToken = user.authentication.idToken; // Safe to send to the server
//    NSString *fullName = user.profile.name;
//    NSArray * array = [fullName componentsSeparatedByString:@" "];
//    
//    NSString * gpfirstName = [array objectAtIndex:0];
//    NSString * gplastName = [array objectAtIndex:1];
//    
////    NSLog(@"ProfileID :%@",fullName);
//    // NSLog(@"first name:%@  lastname:%@",str1,str2);
//    NSString *givenName = user.profile.givenName;
//    NSString *familyName = user.profile.familyName;
//    NSString *email = user.profile.email;
//    NSString *loginType = @"gp";
//    
//    NSLog(@"EmailID:%@",email);
//    
//    [self socialFacebookOrGooglePostMethod:gpfirstName last:gplastName emails:email id:userId logintype:loginType];
//    
//            // ...
//}

- (void)signInWillDispatch:(GIDSignIn *)signIn error:(NSError *)error {
    //  [myActivityIndicator stopAnimating];
    
    NSLog(@"&&&&&&&&&&&&&&&");
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *loginType = [defaults objectForKey:@"loginType"];
    if ([loginType isEqualToString:@"gp"]) {
        NSString *gpFirstName = [defaults objectForKey:@"gpFirstName"];
        NSString *gpLastName = [defaults objectForKey:@"gpLastName"];
        NSString *gpEmail = [defaults objectForKey:@"gpemail"];
        NSString *gpID = [defaults objectForKey:@"gpID"];
        
        loginInteger = 1;
        [defaults setInteger:loginInteger forKey:@"isloggedIn"];
        [defaults synchronize];
        
        [self socialFacebookOrGooglePostMethod:gpFirstName last:gpLastName emails:gpEmail id:gpID logintype:loginType];
        
    }
    
}

// Present a view that prompts the user to sign in with Google
- (void)signIn:(GIDSignIn *)signIn
presentViewController:(UIViewController *)viewController {
    [self presentViewController:viewController animated:YES completion:nil];
    NSLog(@"%%%%%%%%%%%%%%%%%%");
}

// Dismiss the "Sign in with Google" view
- (void)signIn:(GIDSignIn *)signIn
dismissViewController:(UIViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
    NSLog(@"***************");
}

//


- (IBAction)loginButtonAction:(id)sender
{
    if ([usernameTF.text isEqualToString:@""] || [passwordTF.text isEqualToString:@""]) {
        
        alertForFields=   [UIAlertController
                                      alertControllerWithTitle:@"Sign in Failed!"
                                      message:@"Please enter email and Password"
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        if (![usernameTF.text isEqualToString:@""]) {
            
            alertForFields=   [UIAlertController
                               alertControllerWithTitle:@"Sign in Failed!"
                               message:@"Please enter Password"
                               preferredStyle:UIAlertControllerStyleAlert];

        }
        
        if (![passwordTF.text isEqualToString:@""]) {
            
            alertForFields=   [UIAlertController
                               alertControllerWithTitle:@"Sign in Failed!"
                               message:@"Please enter email"
                               preferredStyle:UIAlertControllerStyleAlert];
            
        }

        UIAlertAction* okButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
                                        //Handel your yes please button action here
                                        
                                        
                                    }];
        [alertForFields addAction:okButton];
        
        [self presentViewController:alertForFields animated:YES completion:nil];
    }
    else{
        
        
        [self postMethod];
        
    
 
    }
}








-(void)socialFacebookOrGooglePostMethod:(NSString *)firstname last:(NSString *)lastName emails:(NSString *)emailId id:(NSString *)fbID logintype:(NSString *)type
{
    
    [self.connection cancel];
    
    //initialize new mutable data
    NSMutableData *data = [[NSMutableData alloc] init];
    self.receivedData = data;
    
    NSURL *url = [NSURL URLWithString:(API_URL @"sociallogin")];
    //initialize a request from url
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL]];
    
    //set http method
    [request setHTTPMethod:@"POST"];
    //initialize a post data
    
    
   
    postData = [NSString stringWithFormat:@"email=%@ & social_id=%@ & firstname=%@ & lastname=%@ & type=%@",emailId,fbID,firstname,lastName,type];
    
    
   // NSString *postData = [NSString stringWithFormat:@"email=%@&password=%@",usernameTF.text,passwordTF.text];
    //   NSLog(@"username****: %@  pass**** :%@",usernameTF.text,passwordTF.text);
    
    
    [request setValue:@"application/x-www-form-urlencoded; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    
    [request setHTTPBody:[postData dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    self.connection = connection;
    
    //start the connection
    [connection start];
    
    
}











-(void)postMethod
{
    
    [self.connection cancel];
    
    //initialize new mutable data
    NSMutableData *data = [[NSMutableData alloc] init];
    self.receivedData = data;
    
    NSURL *url = [NSURL URLWithString:(API_URL @"login")]; //http://192.168.1.22/outnabout/webservices/users/login/
    
    //initialize a request from url
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL]];
    
    //set http method
    [request setHTTPMethod:@"POST"];
    //initialize a post data
    NSString *postData = [NSString stringWithFormat:@"email=%@&password=%@",usernameTF.text,passwordTF.text];
  //   NSLog(@"username****: %@  pass**** :%@",usernameTF.text,passwordTF.text);

    
    [request setValue:@"application/x-www-form-urlencoded; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    
    [request setHTTPBody:[postData dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    self.connection = connection;
    
    //start the connection
    [connection start];
    
}





#pragma mark - NSURLConnection delegate methods =  ==  == = = = = = = =  = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    [self.receivedData appendData:data];
}


-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    
    NSLog(@"eeeeeeee  %@" , error);
}


-(void)connectionDidFinishLoading:(NSURLConnection *)connection{
    
    //initialize convert the received data to string with UTF8 encoding
    NSString *htmlSTR = [[NSString alloc] initWithData:self.receivedData
                                              encoding:NSUTF8StringEncoding];
   // NSLog(@" ffff     %@" , htmlSTR);
    
    
    NSError *error = nil;
    NSDictionary *jsonArray = [NSJSONSerialization JSONObjectWithData: self.receivedData options: NSJSONReadingMutableContainers error: &error];  //I am using sbjson to parse
    
    //NSLog(@"dataaaaaaaa %@",jsonArray);  //here is your output
    
    NSString *response = [jsonArray[@"code"] stringValue];
    data = jsonArray[@"data"];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *loginType = [defaults objectForKey:@"loginType"];
    if ([loginType isEqualToString:@"fb"]) {
        
        accesstoken = [jsonArray valueForKey:@"access_token"];
        
    }
    else if ([loginType isEqualToString:@"gp"]) {
        
        accesstoken = [jsonArray valueForKey:@"access_token"];
    }
    
    
    else{
    
    firstName =[data valueForKey:@"firstname"];
    lastName =[data valueForKey:@"lastname"];
    email =[data valueForKey:@"email"];
    accesstoken =[data valueForKey:@"access_token"];
    
    NSLog(@"%@ %@ %@ %@ ********%@",response,firstName,lastName,email,accesstoken);
    }
    if ([response isEqualToString:@"200"]) {
        NSLog(@"Login successfully");
        
        
        NSString *userName = [usernameTF text];
        NSString *passWord  = [passwordTF text];
        
        NSString *first_Name = firstName;
        NSString *last_Name  = lastName;
        NSString *_email = email;
        NSString *access_tokenn  = accesstoken;
        loginInteger = 1;
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        [defaults setObject:userName forKey:@"username"];
        [defaults setObject:passWord forKey:@"password"];
        
        [defaults setObject:first_Name forKey:@"first_name"];
        [defaults setObject:last_Name forKey:@"last_name"];
        
        [defaults setObject:_email forKey:@"_email"];
        [defaults setObject:access_tokenn forKey:@"access_token"];
        
        [defaults setInteger:loginInteger forKey:@"isloggedIn"];
        
        [defaults synchronize];
        
        NSLog(@"Data saved");
        NSLog(@"username: %@  pass :%@ access_Token :%@",userName,passWord,access_tokenn);
        
        //    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        //    ViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"viewcontroller"];
        //    [self.navigationController pushViewController:viewController animated:YES];
        [self dismissViewControllerAnimated:YES completion:nil];
        
        //  [self performSegueWithIdentifier:@"toViewController" sender:nil];

    }
    
    
    else if([response isEqualToString:@"406"])
        
    NSLog(@"Failed to login");
    loginInteger = 0;
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Sign in Failed!"
                                  message:@"Invalid email and password"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   //Handel your yes please button action here
                                   
                                   
                               }];
    [alert addAction:okButton];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}


-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}


- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [[self view] endEditing:YES];
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
