//
//  FavoriteEventsVC.h
//  OutNAbout
//
//  Created by Ram Kumar on 06/07/16.
//  Copyright © 2016 tecHindustan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FavoriteEventsVC : UIViewController

@property (strong, nonatomic) IBOutlet UIBarButtonItem *barButtonItem;

@property (weak, nonatomic) IBOutlet UITableView *imagesTableView;

@property (retain, nonatomic) NSURLConnection *connection;

@property (retain, nonatomic) NSMutableData *receivedData;
@end
