//
//  CreateEventVC.h
//  OutNAbout
//
//  Created by Ram Kumar on 29/06/16.
//  Copyright © 2016 tecHindustan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CreateEventVC : UIViewController


@property (strong, nonatomic) IBOutlet UIBarButtonItem *barButtonItem;

@property (strong, nonatomic)  UIBarButtonItem *rightBarButtonItem;

@property (weak, nonatomic) IBOutlet UIButton *galleryButton;
@property (weak, nonatomic) IBOutlet UIImageView *galleryImageView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UITextField *categoriesTF;
@property(nonatomic,strong) NSMutableArray *selectedItems;
@end
